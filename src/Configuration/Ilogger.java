package Configuration;

public interface Ilogger {
	public void LogSuccess(String LogGroup, String message);

	public void LogFailure(String LogGroup, String message);
	public void LogError(String LogGroup, String message);
	public void LogError(String LogGroup, Exception ex);
	public void LogSuccess(String message);

	public void LogFailure(String message);
	public void LogError(String message);
	public void LogError( Exception ex);
	


}
