package Configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import com.relevantcodes.extentreports.ExtentReports;

import com.google.common.reflect.TypeToken;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;


public class GlobalObjects {
	//public static WebDriver driver;
		public static Connection dbConn;
		//public static ArrayList<ClassMethodPermission> MethodPermission;
		public static Properties prop =null ;
		public static String EnvironmentName;
		public static ExtentReports globalExtentReport;
		public static ExtentTest globalExtentTest;
		public static Ilogger extentLogger;
		/**
		 * Initialize Global objects to use in application anytime 
		 * These are static objects that can be used in application 
		 * @throws FileNotFoundException
		 * @throws IOException
		 * @throws ClassNotFoundException
		 * @throws SQLException
		 */
		public static void InitApp() throws FileNotFoundException, IOException, ClassNotFoundException, SQLException{
			if(prop == null){
				prop= new Properties();
				prop.load(new FileInputStream("C:\\Users\\schanna\\eclipse-workspace\\Appium_Hope\\src\\Configuration\\Properties"));
			}
			if(dbConn == null){
			    Class.forName("org.postgresql.Driver");			
			    dbConn = DriverManager.getConnection(prop.getProperty("dbURL"));
			}
		
		  if(GlobalObjects.EnvironmentName == null)
		  { 
			  GlobalObjects.EnvironmentName=prop.getProperty("Environment"); 
		  } 
		  
		/*
		 * if(GlobalObjects.MethodPermission ==null)
		 * 
		 * { Type listType = new TypeToken<ArrayList<ClassMethodPermission>>() { private
		 * static final long serialVersionUID = 1L; }.getType();
		 * 
		 * GlobalObjects.MethodPermission= new Gson().fromJson(new FileReader(
		 * "C:\\NYC\\Selenium_Workout\\Maven_BCS\\bcs-selenium\\Code\\src\\resources\\BlockedMethods.json"
		 * ),listType); System.out.println("Json Parsed");
		 * 
		 * }
		 */ 
		  GlobalObjects.extentLogger = new ExtentLogger();
		 
			
			//Excel_Read.getFilterList();
			
		}
		

}
