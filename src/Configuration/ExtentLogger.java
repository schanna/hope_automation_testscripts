package Configuration;

import com.relevantcodes.extentreports.LogStatus;

public class ExtentLogger implements Ilogger{
	@Override
	public  void LogSuccess(String LogGroup, String message) {
		if(GlobalObjects.globalExtentTest != null && GlobalObjects.globalExtentTest.getTest().getName().equals(LogGroup)){
			GlobalObjects.globalExtentTest.log(LogStatus.PASS, message);
		
		}
		else{
			GlobalObjects.globalExtentTest= GlobalObjects.globalExtentReport.startTest(LogGroup);
			GlobalObjects.globalExtentTest.log(LogStatus.PASS, message);
		}
	}

	@Override
	public void LogFailure(String LogGroup, String message) {
		if(GlobalObjects.globalExtentTest != null && GlobalObjects.globalExtentTest.getTest().getName().equals(LogGroup)){
			GlobalObjects.globalExtentTest.log(LogStatus.FAIL, message);
			//RestAssured.Log_Jira_Issue(LogGroup, message);
		}
		else{
			GlobalObjects.globalExtentTest= GlobalObjects.globalExtentReport.startTest(LogGroup);
			GlobalObjects.globalExtentTest.log(LogStatus.FAIL, message);
		}
	}

	@Override
	public void LogError(String LogGroup, String message) {
		if(GlobalObjects.globalExtentTest != null && GlobalObjects.globalExtentTest.getTest().getName().equals(LogGroup)){
			GlobalObjects.globalExtentTest.log(LogStatus.ERROR, message);
		}
		else{
			GlobalObjects.globalExtentTest= GlobalObjects.globalExtentReport.startTest(LogGroup);
			GlobalObjects.globalExtentTest.log(LogStatus.ERROR, message);
		}
	}

	@Override
	public void LogError(String LogGroup, Exception ex) {
		if(GlobalObjects.globalExtentTest != null && GlobalObjects.globalExtentTest.getTest().getName().equals(LogGroup)){
			GlobalObjects.globalExtentTest.log(LogStatus.ERROR, ex.getMessage());
		}
		else{
			GlobalObjects.globalExtentTest= GlobalObjects.globalExtentReport.startTest(LogGroup);
			GlobalObjects.globalExtentTest.log(LogStatus.ERROR, ex.getMessage());
		}
	}

	@Override
	public void LogSuccess(String message) {
		GlobalObjects.globalExtentTest.log(LogStatus.PASS, message);
		
	}

	@Override
	public void LogFailure(String message) {
		GlobalObjects.globalExtentTest.log(LogStatus.FAIL, message);
		
	}

	@Override
	public void LogError(String message) {
		GlobalObjects.globalExtentTest.log(LogStatus.ERROR, message);
		
	}

	@Override
	public void LogError(Exception ex) {
		GlobalObjects.globalExtentTest.log(LogStatus.ERROR, ex.getMessage());
		
	}


}
