package main_test;

import org.testng.annotations.Test;

import Configuration.GlobalObjects;

import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import Utils.Common_methods;
import Utils.DBUtils;
import Utils.Objects_elements;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.PerformsTouchActions;

import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;


public class Appium_test extends Base {
	public static String xpath1 = "//android.widget.TextView[@text='Under 18']";
	public static String Query = "select count(*) from \"Submissions\" ";

	@Test(priority = 1)
	public static void Login()
			throws InterruptedException, FileNotFoundException, ClassNotFoundException, IOException, SQLException {
		setCapability();

		
		
		  System.out.println("test pass0"); Thread.sleep(19000);
		  Common_methods.waitUntilElementIsVisible(Objects_elements.UserName(),5);
		  Objects_elements.UserName().click();
		  Objects_elements.UserName().sendKeys("willy165@verizon.net");
		  System.out.println("test pass"); driver.hideKeyboard();
		  System.out.println("test pass1"); //Thread.sleep(10000);
		  Common_methods.waitUntilElementIsVisible(Objects_elements.Sign_in(),5);
		  Objects_elements.Sign_in().click(); System.out.println("test pass2");
		  Thread.sleep(21000);
		  
		  
		  GlobalObjects.extentLogger.LogSuccess(testGroupName, "User login success");
		 
	}

	@Test(priority=2)
	public static void startArea_StartSurvey() throws InterruptedException {
		System.out.println("test pass3");
		Thread.sleep(35000);

		// Common_methods.waitUntilElementIsClickable(Objects_elements.select_area(),
		// 10);
		System.out.println("test pass3_2");
		Objects_elements.select_area1().click();
		System.out.println("test pass3_4");
		Thread.sleep(15000);
		Objects_elements.start_area().click();
		System.out.println("test pass3_5");
		Thread.sleep(15000);
		// Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),5);
		Objects_elements.start_survey().click();
		
			GlobalObjects.extentLogger.LogSuccess(testGroupName, "Area has been started");
		

	}

	@Test(priority=3)
	public static void submit_survey_Decoy() throws InterruptedException, SQLException {
		int actualCount = DBUtils.getScalarCount(Query);
		actualCount = actualCount + 1;

		Thread.sleep(10000);
		// Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),5);
		Objects_elements.Q1_Awake().click();
		Objects_elements.Q2_Yes().click();
		Objects_elements.Q3_Decoy().click();
		Common_methods.waitUntilElementIsVisible(Objects_elements.Q3_Decoy_text(), 5);
		Objects_elements.Q3_Decoy_text().click();
		Thread.sleep(1000);
		Objects_elements.Q3_Decoy_text().sendKeys("M0028");
		Thread.sleep(1000);
		Objects_elements.Submit_survey().click();
		Thread.sleep(10000);
		// Common_methods.waitUntilElementIsClickable(Objects_elements.Confirmation_ok(),10);
		Objects_elements.Confirmation_ok().click();
		// Thread.sleep(10000);
		int expectedCount = DBUtils.getScalarCount(Query);
		System.out.println("value of the query is :" + expectedCount);

		if (actualCount == expectedCount) {
			GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + Integer.toString(actualCount));

		} else {
			GlobalObjects.extentLogger.LogFailure(testGroupName,
					"Actual Result is ::" + Integer.toString(actualCount)
							+ " Expected Result is ::" + Integer.toString(expectedCount));

		}
	}

	@Test(priority=4)
	public static void submit_survey_nonhomeless() throws InterruptedException, SQLException {
		int actualCount_nh = DBUtils.getScalarCount(Query);
		actualCount_nh = actualCount_nh + 1;

		Thread.sleep(10000);
		// driver.findElementByXPath("//android.widget.TextView[@text='START A
		// SURVEY']").click();
		Objects_elements.start_survey().click();
		Thread.sleep(10000);
		System.out.println("test pass6");
		Objects_elements.Q1_Awake().click();
		Objects_elements.Q2_Yes().click();
		Objects_elements.Q3_No().click();
		// Common_methods.waitUntilElementIsVisible(Objects_elements.Q4_Room(),5);
		Thread.sleep(1000);
		Objects_elements.Q4_Room().click();
		Thread.sleep(10000);
		Objects_elements.Submit_survey().click();
		Thread.sleep(10000);
		Objects_elements.Confirmation_ok().click();

		int expectedCount_nh = DBUtils.getScalarCount(Query);
		System.out.println("value of the query is :" + expectedCount_nh);

		if (actualCount_nh == expectedCount_nh) {
			GlobalObjects.extentLogger.LogSuccess(testGroupName,
					"Actual Result is ::" + Integer.toString(actualCount_nh));

		} else {
			GlobalObjects.extentLogger.LogFailure(testGroupName,
					"Actual Result is ::" + Integer.toString(actualCount_nh)
							+ " Expected Result is ::" + Integer.toString(expectedCount_nh));

		}
	}

	@Test(priority = 5)
	public static void submit_survey_homeless() throws Exception {
		Thread.sleep(10000);
		System.out.println("test pass5");

		Thread.sleep(10000);
		Objects_elements.start_survey().click();
		Thread.sleep(10000);
		System.out.println("test pass6");
		Objects_elements.Q1_Awake().click();
		Objects_elements.Q2_Yes().click();
		Objects_elements.Q3_No().click();
		Objects_elements.Q4_Street().click();
		
		Thread.sleep(10000);
		System.out.println("test pass7");
		
		Common_methods.Swipe();
		Thread.sleep(10000);
		Objects_elements.Q6_under_18().click();
		Thread.sleep(10000);
		Common_methods.Swipe();
		Thread.sleep(15000);
		Objects_elements.Q7_Other_Gender().click();
		Thread.sleep(10000);
		Common_methods.Swipe();
		Thread.sleep(10000);
		Objects_elements.Q8_Black().click();
		Thread.sleep(10000);
		Objects_elements.Submit_survey().click();
		Thread.sleep(10000);
		Objects_elements.Confirmation_ok().click();
	}
	
	
	
	@Test(priority = 6)
	public static void Decoy_validation() throws Exception {
		String expected="- Decoy Code";
		Thread.sleep(10000);
		Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),15);
		Objects_elements.start_survey().click();
		Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),15);
		Objects_elements.Q1_Awake().click();
		Objects_elements.Q2_Yes().click();
		Objects_elements.Q3_Decoy().click();
		Objects_elements.Q3_Decoy_text().click();
		Objects_elements.Q3_Decoy_text().sendKeys("Test");
		Objects_elements.Submit_survey().click();
		Common_methods.waitUntilElementIsVisible(Objects_elements.Decoy_validation_message(),15);
		String actual =Objects_elements.Decoy_validation_message().getText();
		Objects_elements.Confirmation_ok().click();
		Common_methods.waitUntilElementIsVisible(Objects_elements.Cancel_survey(),15);
		Objects_elements.Cancel_survey().click();
		Objects_elements.Confirmation_Cancel_survey().click();
		if (actual == expected) {
			GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + actual);

		} else {
			GlobalObjects.extentLogger.LogFailure(testGroupName,
					"Actual Result is ::" + actual
							+ " Expected Result is ::" + expected);
		
		}
	}
		@Test(priority = 7)
		public static void Non_Homeless_DoesNotAnswer() throws Exception {
			int actualCount = DBUtils.getScalarCount(Query);
			actualCount = actualCount + 1;
			Thread.sleep(10000);
			Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),15);
			Objects_elements.start_survey().click();
			Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),15);
			Objects_elements.Q1_Awake().click();
			Objects_elements.Q2_No().click();
			Common_methods.waitUntilElementIsVisible(Objects_elements.Q5_No(),15);
			Objects_elements.Q5_No().click();
			Objects_elements.Submit_survey().click();
			Objects_elements.Confirmation_ok().click();
			int expectedCount = DBUtils.getScalarCount(Query);
			System.out.println("value of the query is :" + expectedCount);

			if (actualCount == expectedCount) {
				GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + Integer.toString(actualCount));

			} else {
				GlobalObjects.extentLogger.LogFailure(testGroupName,
						"Actual Result is ::" + Integer.toString(actualCount)
								+ " Expected Result is ::" + Integer.toString(expectedCount));

			}
	
		}
	

		@Test(priority = 8)
		
			public static void Homeless_DoesNotAnswer() throws Exception {
				int actualCount = DBUtils.getScalarCount(Query);
				actualCount = actualCount + 1;
				Thread.sleep(10000);
				Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),15);
				Objects_elements.start_survey().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),15);
				Objects_elements.Q1_Awake().click();
				Objects_elements.Q2_No().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Q5_No(),15);
				Objects_elements.Q5_Yes().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Q6_under_18(),15);
				Objects_elements.Q6_under_18().click();
				Thread.sleep(10000);
				Common_methods.Swipe();
				Thread.sleep(15000);
				Objects_elements.Q7_Other_Gender().click();
				Thread.sleep(10000);
				Common_methods.Swipe();
				Thread.sleep(10000);
				Objects_elements.Q8_Black().click();
				Thread.sleep(10000);
				Objects_elements.Submit_survey().click();
				Thread.sleep(10000);
				Objects_elements.Submit_survey().click();
				Objects_elements.Confirmation_ok().click();
				int expectedCount = DBUtils.getScalarCount(Query);
				System.out.println("value of the query is :" + expectedCount);

				if (actualCount == expectedCount) {
					GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + Integer.toString(actualCount));

				} else {
					GlobalObjects.extentLogger.LogFailure(testGroupName,
							"Actual Result is ::" + Integer.toString(actualCount)
									+ " Expected Result is ::" + Integer.toString(expectedCount));

				}
	
		}
			@Test(priority = 9)
			
			public static void Submit_without_answering_Q1() throws Exception {	
				String expected="- 1. Is this person:";
				Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),20);
				Objects_elements.start_survey().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),15);
				Common_methods.waitUntilElementIsVisible(Objects_elements.Submit_survey(),15);
				Objects_elements.Submit_survey().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_validation_message(),15);
				String actual =Objects_elements.Q1_validation_message().getText();
				Objects_elements.Confirmation_ok().click();
				Common_methods.tap();
				Objects_elements.Q1_Awake().click();
				Objects_elements.Q2_Yes().click();
			    Objects_elements.Q3_Yes().click();
				Objects_elements.Submit_survey().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Confirmation_ok(),15);
				Objects_elements.Confirmation_ok().click();
				
						
		  if (expected.contains(actual))
		  { 
			  GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + actual);
		  } 
		  else 
		  {
			  GlobalObjects.extentLogger.LogFailure(testGroupName, "Actual Result is ::" + actual + " Expected Result is ::" + expected);
		  }

	}
          @Test(priority = 10)
			
			public static void Submit_without_answering_Q2() throws Exception {
				//String windowHandle=driver.getWindowHandle();
				
				String expected="- 2. May I ask you just a few questions?";
				Thread.sleep(10000);
				Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),15);
				Objects_elements.start_survey().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),15);
				Objects_elements.Q1_Awake().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Submit_survey(),15);
				
				Objects_elements.Submit_survey().click();
				
				Common_methods.waitUntilElementIsVisible(Objects_elements.Q2_validation_message(),15);
				String actual =Objects_elements.Q2_validation_message().getText();
				Objects_elements.Confirmation_ok().click();
				Common_methods.tap();
				System.out.println("TEst1");
				System.out.println(actual);
				Thread.sleep(1000);
				
					Objects_elements.Q2_Yes().click();
					System.out.println("TEst13");
			Objects_elements.Q3_Yes().click();
				Objects_elements.Submit_survey().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Confirmation_ok(),15);
				Objects_elements.Confirmation_ok().click();
				
						
		  if (expected.contains(actual))
		  { 
			  GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + actual);
		  } 
		  else 
		  {
			  GlobalObjects.extentLogger.LogFailure(testGroupName, "Actual Result is ::" + actual + " Expected Result is ::" + expected);
		  }

	}
           @Test(priority = 11)
			
			public static void Submit_without_answering_Q3() throws Exception {	
				String expected="- 3. Did someone else ask you about your housing situation tonight?";
				Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),20);
				Objects_elements.start_survey().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),15);
				Objects_elements.Q1_Awake().click();
				Objects_elements.Q2_Yes().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Submit_survey(),15);
				Objects_elements.Submit_survey().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Q3_validation_message(),15);
				String actual =Objects_elements.Q3_validation_message().getText();
				Objects_elements.Confirmation_ok().click();
				Common_methods.tap();
			    Objects_elements.Q3_Yes().click();
				Objects_elements.Submit_survey().click();
				Common_methods.waitUntilElementIsVisible(Objects_elements.Confirmation_ok(),15);
				Objects_elements.Confirmation_ok().click();
				
						
		  if (expected.contains(actual))
		  { 
			  GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + actual);
		  } 
		  else 
		  {
			  GlobalObjects.extentLogger.LogFailure(testGroupName, "Actual Result is ::" + actual + " Expected Result is ::" + expected);
		  }

	}
			 @Test(priority = 12)
				
				public static void Submit_without_answering_Q4() throws Exception {	
					String expected="- 4. Where are you sleeping tonight?";
					Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),20);
					Objects_elements.start_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),15);
					Objects_elements.Q1_Awake().click();
					Objects_elements.Q2_Yes().click();
					Objects_elements.Q3_No().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Submit_survey(),15);
					Objects_elements.Submit_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q4_validation_message(),15);
					String actual =Objects_elements.Q4_validation_message().getText();
					Objects_elements.Confirmation_ok().click();
					Common_methods.tap();
				    Objects_elements.Q4_Room().click();
					Objects_elements.Submit_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Confirmation_ok(),15);
					Objects_elements.Confirmation_ok().click();
					
							
			  if (expected.contains(actual))
			  { 
				  GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + actual);
			  } 
			  else 
			  {
				  GlobalObjects.extentLogger.LogFailure(testGroupName, "Actual Result is ::" + actual + " Expected Result is ::" + expected);
			  }

		}
			 @Test(priority = 13)
				
				public static void Submit_without_answering_Q5() throws Exception {	
					String expected="- 5. Is this person street homeless?";
					Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),20);
					Objects_elements.start_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),15);
					Objects_elements.Q1_Awake().click();
					Objects_elements.Q2_Yes().click();
					Objects_elements.Q3_No().click();
					Common_methods.Swipe();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q4_Dont_Know(),15);
					Objects_elements.Q4_Dont_Know().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Submit_survey(),15);
					Objects_elements.Submit_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q5_validation_message(),15);
					String actual =Objects_elements.Q5_validation_message().getText();
					Objects_elements.Confirmation_ok().click();
					Common_methods.Swipe();					
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q5_No(),15);
				    Objects_elements.Q5_No().click();
					Objects_elements.Submit_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Confirmation_ok(),15);
					Objects_elements.Confirmation_ok().click();
					
							
			  if (expected.contains(actual))
			  { 
				  GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + actual);
			  } 
			  else 
			  {
				  GlobalObjects.extentLogger.LogFailure(testGroupName, "Actual Result is ::" + actual + " Expected Result is ::" + expected);
			  }

		}
			 @Test(priority = 14)
				
				public static void Submit_without_answering_Q6() throws Exception {	
					String expected="- 6. What is your age?";
					Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),20);
					Objects_elements.start_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),15);
					Objects_elements.Q1_Awake().click();
					Objects_elements.Q2_Yes().click();
					Objects_elements.Q3_No().click();
					Common_methods.Swipe();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q4_Dont_Know(),15);
					Objects_elements.Q4_Dont_Know().click();
					Objects_elements.Q5_Yes().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Submit_survey(),15);
					Objects_elements.Submit_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q5_validation_message(),15);
					String actual =Objects_elements.Q5_validation_message().getText();
					Objects_elements.Confirmation_ok().click();
					Common_methods.Swipe();					
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q5_No(),15);
				    Objects_elements.Q5_No().click();
					Objects_elements.Submit_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Confirmation_ok(),15);
					Objects_elements.Confirmation_ok().click();
					
							
			  if (expected.contains(actual))
			  { 
				  GlobalObjects.extentLogger.LogSuccess(testGroupName, "Actual Result is ::" + actual);
			  } 
			  else 
			  {
				  GlobalObjects.extentLogger.LogFailure(testGroupName, "Actual Result is ::" + actual + " Expected Result is ::" + expected);
			  }

			 
		}
			 @Test(priority = 15)
			 public static void start2Area_submitSurvey() throws InterruptedException, SQLException {
				 Common_methods.waitUntilElementIsVisible(Objects_elements.Finish_Area(),15);
				 Objects_elements.Finish_Area().click();
				 Objects_elements.Confirmation_Yes().click();
				 int actualCount_nh = DBUtils.getScalarCount(Query);
					actualCount_nh = actualCount_nh + 1;
					Common_methods.waitUntilElementIsVisible(Objects_elements.select_area2(),15);
					Objects_elements.select_area2().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.start_area(),15);
					Objects_elements.start_area().click();	
					Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),5);
					Objects_elements.start_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),5);
					Objects_elements.Q1_Awake().click();
					Objects_elements.Q2_Yes().click();
					Objects_elements.Q3_No().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q4_Room(),5);
					Objects_elements.Q4_Room().click();
					Objects_elements.Submit_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Confirmation_ok(),15);
					Objects_elements.Confirmation_ok().click();

					int expectedCount_nh = DBUtils.getScalarCount(Query);
					System.out.println("value of the query is :" + expectedCount_nh);

					if (actualCount_nh == expectedCount_nh) {
						GlobalObjects.extentLogger.LogSuccess(testGroupName,
								"Actual Result is ::" + Integer.toString(actualCount_nh));

					} else {
						GlobalObjects.extentLogger.LogFailure(testGroupName,
								"Actual Result is ::" + Integer.toString(actualCount_nh)
										+ " Expected Result is ::" + Integer.toString(expectedCount_nh));

					}
				}

			 @Test(priority = 16)
			 public static void start3Area_submitSurvey() throws InterruptedException, SQLException {
				 Common_methods.waitUntilElementIsVisible(Objects_elements.Finish_Area(),15);
				 Objects_elements.Finish_Area().click();
				 Objects_elements.Confirmation_Yes().click();
				 int actualCount_nh = DBUtils.getScalarCount(Query);
					actualCount_nh = actualCount_nh + 1;
					Common_methods.waitUntilElementIsVisible(Objects_elements.select_area2(),15);
					Objects_elements.select_area3().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.start_area(),15);
					Objects_elements.start_area().click();	
					Common_methods.waitUntilElementIsVisible(Objects_elements.start_survey(),5);
					Objects_elements.start_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q1_Awake(),5);
					Objects_elements.Q1_Awake().click();
					Objects_elements.Q2_Yes().click();
					Objects_elements.Q3_No().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Q4_Room(),5);
					Objects_elements.Q4_Room().click();
					Objects_elements.Submit_survey().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Confirmation_ok(),15);
					Objects_elements.Confirmation_ok().click();
					Common_methods.waitUntilElementIsVisible(Objects_elements.Finish_Area(),15);
					 Objects_elements.Finish_Area().click();

					int expectedCount_nh = DBUtils.getScalarCount(Query);
					System.out.println("value of the query is :" + expectedCount_nh);

					if (actualCount_nh == expectedCount_nh) {
						GlobalObjects.extentLogger.LogSuccess(testGroupName,
								"Actual Result is ::" + Integer.toString(actualCount_nh));

					} else {
						GlobalObjects.extentLogger.LogFailure(testGroupName,
								"Actual Result is ::" + Integer.toString(actualCount_nh)
										+ " Expected Result is ::" + Integer.toString(expectedCount_nh));

					}
				}
			 @Test(priority = 17)
			 public static void Refresh()
			 {
				 Common_methods.waitUntilElementIsVisible(Objects_elements.Options(),15);
				 Objects_elements.Options().click();
				 Common_methods.waitUntilElementIsVisible(Objects_elements.Options_Refresh(),10);
				 Objects_elements.Options_Refresh().click();
				 GlobalObjects.extentLogger.LogSuccess(testGroupName, "Refresh success");
				 
			 }
			 @Test(priority = 18)
			 public static void Cancel()
			 {
				 Common_methods.waitUntilElementIsVisible(Objects_elements.Options(),15);
				 Objects_elements.Options().click();
				 Common_methods.waitUntilElementIsVisible(Objects_elements.Options_Cancel(),10);
				 Objects_elements.Options_Cancel().click();
				 GlobalObjects.extentLogger.LogSuccess(testGroupName, "Cancel success");
				 
			 }
			 @Test(priority = 19)
			 public static void Logout()
			 {
				 Common_methods.waitUntilElementIsVisible(Objects_elements.Options(),15);
				 Objects_elements.Options().click();
				 Common_methods.waitUntilElementIsVisible(Objects_elements.Options_Logout(),10);
				 Objects_elements.Options_Logout().click();
				 GlobalObjects.extentLogger.LogSuccess(testGroupName, "Logout success");
				 
			 }
			 


}