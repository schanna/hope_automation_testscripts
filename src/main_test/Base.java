package main_test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;


import Configuration.GlobalObjects;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Base {
	public static String testGroupName;
	public static void InitApp() throws FileNotFoundException, IOException, ClassNotFoundException, SQLException{
		if(GlobalObjects.dbConn == null){
			GlobalObjects.InitApp();
		}
		if(GlobalObjects.globalExtentReport == null ){
			GlobalObjects.globalExtentReport = new ExtentReports("C:\\Users\\schanna\\eclipse-workspace\\Appium_Hope\\TestSummary.html", true);
		GlobalObjects.globalExtentReport.loadConfig(new File("C:\\Users\\schanna\\eclipse-workspace\\Appium_Hope\\src\\Utils\\ExtentConfig.xml"));
		}
		
	}	
	@BeforeTest
	public void setUp() throws ClassNotFoundException, SQLException, IOException
	{
		Base.InitApp();
		GlobalObjects.globalExtentReport.loadConfig(new File("C:\\Users\\schanna\\eclipse-workspace\\Appium_Hope\\extent-config.xml"));
		GlobalObjects.globalExtentReport.startTest("Hope Automation Framework");
	}

 @BeforeMethod
 public void beforeMethod(Method method)
 {
	 Base.testGroupName=this.getClass().getSimpleName()+" : " + method.getName();
	 GlobalObjects.globalExtentTest = GlobalObjects.globalExtentReport.startTest(Base.testGroupName,method.getName());
	 GlobalObjects.globalExtentTest.assignAuthor("Sowbhagya Channa");
	 GlobalObjects.globalExtentTest.assignCategory(GlobalObjects.EnvironmentName);
	 
	 
 }
 
 
	
	//public static AppiumDriver<MobileElement> driver;
	public static AndroidDriver driver;
	public static AppiumDriver<MobileElement> setCapability() throws InterruptedException, FileNotFoundException, ClassNotFoundException, IOException, SQLException 
	{
	//Set the Desired Capabilities
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("deviceName", "Test");
			caps.setCapability("udid", "emulator-5554"); //Give Device ID of your mobile phone
			caps.setCapability("platformName", "Android");
			//caps.setCapability("platformVersion", "6.0");
			caps.setCapability("automationName", "UiAutomator1");
			caps.setCapability("appPackage", "gov.nyc.dhs.oit.mobile.Hope2020.development");
			caps.setCapability("appActivity", "com.hope2020.MainActivity");
			caps.setCapability("noReset", "true");
			try {
				 //driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), caps);
				 driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
				 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			} catch (MalformedURLException e) {
						System.out.println(e.getMessage());
}
			
	return driver;
	}
	@AfterMethod
	 public void afterMethod(Method method)
	 {
		 //GlobalObjects.globalExtentReport.flush();
		// System.out.println("Inside After method");
		 GlobalObjects.globalExtentReport.endTest(GlobalObjects.globalExtentTest);
		 
		 
	 }
	 
		@AfterTest()
		public void closeConnection() throws SQLException, InterruptedException
		{
			//driver.close();
			//driver.quit();
			//dbConn.close();
			GlobalObjects.globalExtentReport.flush();
			GlobalObjects.globalExtentReport.close();
			//driver.closeApp();
			
		}
		//@AfterClass()
		public void close_app()
		{
			driver.closeApp();
			GlobalObjects.globalExtentReport.flush();
			GlobalObjects.globalExtentReport.close();
		}
	}
