package Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Configuration.GlobalObjects;

public class DBUtils {
	
	public static ResultSet getResultSet (String query) throws  SQLException
	{

		//Create Statement Object		
		Statement stmt = GlobalObjects.dbConn.createStatement();					

		// Execute the SQL Query. Store results in ResultSet		
		ResultSet rs= stmt.executeQuery(query);
		return rs;	

	}
	
	
	public static int getScalarCount (String query) throws  SQLException
	{
		int returnValue=0;
		//Create Statement Object		
		Statement stmt = GlobalObjects.dbConn.createStatement();					

		// Execute the SQL Query. Store results in ResultSet		
		ResultSet rs= stmt.executeQuery(query);
		while(rs.next()){
			returnValue =rs.getInt(1);
		}
		return returnValue;	

	}


	

}
