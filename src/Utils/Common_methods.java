package Utils;

import java.awt.Dimension;
import java.awt.Point;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import main_test.Base;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class Common_methods extends Base {

	public static void scrollDowntoXPath(String xPath, AppiumDriver driver) {
	    boolean flag=true;
	    int count=1;
	    while(flag){
	        try {
	            driver.findElement(By.xpath(xPath));
	            flag=false;
	            break;
	        }
	        catch(Exception NoSuchElementException) {
	            count=count+1;
	            Map<String, Object> params = new HashMap<>();
	            params.put("start","40%,90%");
	            params.put("end","40%,20%");
	            params.put("duration","2");
	            Object res= driver.executeScript("mobile:touch:swipe",params);
	        if(count==5)
	        {
	            break;
	        }
	        }
	    }
	}
	
	/**
	 * Wait until webedit box is clickable and set text in
	 * @param element
	 * @param inputvalue
	 * @param seconds
	 * @param waitUntilClickable
	 */
	public static void waitAndSetText(WebElement element, String inputvalue, int seconds, boolean waitUntilClickable)
	{
		if (inputvalue == null) return;

		if (waitUntilClickable)
		{
			waitUntilElementIsClickable(element, seconds);
		}

		element.clear();

		Actions actions = new Actions(driver);
		actions.moveToElement(element).click();

		actions.sendKeys(inputvalue);
		actions.build().perform();
	}
	
	/**Scrolling ALL WAy Down to the page using Javascript Executor */
	public static void ScrollDown( )
	{
		JavascriptExecutor js = (JavascriptExecutor) driver; 
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	}

	/**Scrolling ALL Way Up to the page using Javascript Executor */
	public static void ScrollUp()
	{

		JavascriptExecutor js = (JavascriptExecutor) driver;  
		js.executeScript("window.scrollBy(0,-document.body.scrollHeight)");
	}

	//=======================Wait==================

	/**
	 * Globalwait/ wait for all web elements in webpage to be loaded
	 * @param seconds
	 */
	public static void implictWait(int seconds){
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}


	/**
	 * Wait until an specific web element to be visible in the webpage
	 * @param element
	 * @param time
	 */
	public static void waitUntilElementIsVisible(WebElement element, int time )
	{
		new WebDriverWait (driver, time).until(ExpectedConditions.visibilityOf(element));

	}

	public static void fluentWaitUntilElementIsVisible(WebElement element )
	{
		Wait<WebDriver> wait = fluentWait();
		wait.until(ExpectedConditions.visibilityOf(element));

	}

	/**
	 * Wait until an specific web element to be Clickable in the webpage
	 * @param element
	 * @param time
	 */
	public static void waitUntilElementIsClickable(WebElement element, int time )
	{
		new WebDriverWait (driver, time).until(ExpectedConditions.elementToBeClickable(element));

	}

	public static Wait<WebDriver> fluentWait()
	{
		return new FluentWait<WebDriver>(driver)
				.withTimeout(Duration.ofSeconds(45))
				.pollingEvery(Duration.ofSeconds(5))
				.ignoring(WebDriverException.class)
				.ignoring(NoSuchElementException.class)
				.ignoring(Exception.class);

	}

	public static void fluentWaitUntilElementIsClickable(WebElement element)
	{
		fluentWait().until(ExpectedConditions.elementToBeClickable(element));
	}


	/**
	 * Wait until an specific web element's innertext contains a value in the webpage
	 * @param element
	 * @param text
	 * @param time
	 */
	public static void waitUntilElementContainsText(WebElement element, String text, int time )
	{		
		new WebDriverWait (driver, time).until(ExpectedConditions.textToBePresentInElement(element, text));
	}

	/**
	 * Wait until an specific web element to be invisible in the webpage
	 * @param element
	 * @param time
	 */
	public static void waitUntilElementIsInvisible(WebElement element,int time )
	{		
		new WebDriverWait (driver, time).until (ExpectedConditions.invisibilityOf(element));//(element.getText()!=text);
	}
	
	protected WebElement WaitUntilElementIsDisplayed(By by, int seconds)
    {
        try
        {
            WebDriverWait wait = new WebDriverWait(driver,10);
            return wait.ignoring(NoSuchElementException.class)
                    .pollingEvery(Duration.ofSeconds(1))
                    .until(ExpectedConditions.visibilityOfElementLocated(by));
        }
        catch (Exception ex)
        {
            System.out.println("Exception while waiting {ex.Message}" + ex.getMessage());
            return null;
        }
    }

	/**
	 * Wait until an specific webelements' list to be visible in the webpage
	 * @param element
	 * @param time
	 */
	public static void waitUntilElementsAreInvisible(List<WebElement> elements,int time )
	{		
		new WebDriverWait (driver, time).until (ExpectedConditions.invisibilityOfAllElements(elements));//(element.getText()!=text);
	}

	/**
	 * Checking if the element is present or not
	 * @return True if the element is present, false otherwise.
	 */
	public static boolean elementPresent( By by)
	{
		try
		{
			driver.findElement(by);
			return true;
		}
		catch (NoSuchElementException e )
		{
			return false;
		}
	}

	public static void keepWaitUntilElementPresentByLocators( By by)
	{
		while (!elementPresent(by))
		{
			fluentWait().until (ExpectedConditions.visibilityOfElementLocated(by));
		}
	}


	
	  public static void swipeDown(int pixelsToSwipe) {
	  
	  try { 
      org.openqa.selenium.Point value = null;
	  value = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView")).getLocation(); 
	  int x =value.x; 
	  System.out.println("int value X  is "+x);
	  int y = value.y; 
	  System.out.println("int value Y  is "+y);
	  int y1 = value.y+pixelsToSwipe;
	  
	  swipe(x,y1,x,y);
	  
	  } catch(Exception e) { System.out.println(e.getMessage()); }
	  
	  } 
	  
	  public static void swipe(int fromX,int fromY,int toX,int toY) {
	  
	  TouchAction action = new TouchAction(driver);
	  action.press(PointOption.point(fromX,fromY)).waitAction(new WaitOptions().withDuration(Duration.ofMillis(3000))).release().perform();
	  //you can change wait durations as per your requirement .moveTo(PointOption.point(toX, toY))
	   }
	/*
	 * //Generic function for Scroll public static void
	 * scrollUsingTouchActions_ByElements(MobileElement src, MobileElement dest) {
	 * TouchAction actions = new TouchAction(driver);
	 * actions.longPress(src).waitAction().moveTo(dest).release().perform(); }
	 */
	  
	  public static void tap() {
			new TouchAction((PerformsTouchActions) driver)
			.press(PointOption.point(80, 800))
			.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
			.moveTo(PointOption.point(80, 600)).release().perform();
			
	    }
	  public static void Swipe() throws InterruptedException
		{
			new TouchAction((PerformsTouchActions) driver)
			.press(PointOption.point(240, 1648))
			.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
			.moveTo(PointOption.point(240, 1076))
			.release().perform();
			
			Thread.sleep(10000);
			
			new TouchAction((PerformsTouchActions) driver)
			.press(PointOption.point(240, 1076))
			.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
			.moveTo(PointOption.point(240, 800))
			.release().perform();
			
			Thread.sleep(10000);
			
			new TouchAction((PerformsTouchActions) driver)
			.press(PointOption.point(240, 800))
			.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
			.moveTo(PointOption.point(240, 300))
			.release().perform();
			Thread.sleep(10000);
		}
	  

}
