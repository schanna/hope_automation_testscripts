package Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import main_test.Base;

public class Objects_elements extends Base {

	
	public static WebElement UserName()
	{
		return driver.findElement(By.xpath("//android.widget.EditText[@class='android.widget.EditText']"));
	}
	
	public static WebElement Sign_in()
	{
		return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.view.ViewGroup[2]"));
	}
	
	public static WebElement select_area1()
	{
		return driver.findElement(By.xpath("//android.view.View[@content-desc=\"Google Map\"]/android.view.View[2]"));
	}
	public static WebElement select_area2()
	{
		return driver.findElement(By.xpath("//android.view.View[@content-desc=\"Google Map\"]/android.view.View[3]"));
	}
	public static WebElement select_area3()
	{
		return driver.findElement(By.xpath("//android.view.View[@content-desc=\"Google Map\"]/android.view.View[1]"));
	}
	public static WebElement start_area()
	{
		return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"));
	}
	public static WebElement start_survey()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='START A SURVEY']"));
	}
	
	public static WebElement Finish_Area()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='FINISH AREA']"));
	}
	
	public static WebElement Q1_Awake()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Awake']"));
	}
	public static WebElement Q2_Yes()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Yes']"));
	}
	public static WebElement Q2_No()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='No']"));
	}
	public static WebElement Q3_No()
	{
		return driver.findElement(By.xpath("(//android.widget.TextView[@text='No'])[2]"));
	}
	public static WebElement Q3_Yes()
	{return driver.findElement(By.xpath("(//android.widget.TextView[@text='Yes'])[2]"));
		//return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView"));
											

	}
	public static WebElement Q3_Decoy()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Decoy']"));
		//android.widget.TextView[@text='Room // Apartment // House // Hotel // Dorm // Drop-in Center // Shelter // Safe Haven // Residential Program // Other (NOT STREET HOMELESS)']
	}
	public static WebElement Q3_Decoy_text()
	{
		return driver.findElement(By.xpath("//android.widget.EditText[@NAF='true']"));
	}
	public static WebElement Q4_Room()
	{
		return driver.findElement(By.xpath("//*[contains(@text,'Room')]"));
	}
	public static WebElement Q4_Street()
	{
		return driver.findElement(By.xpath("//*[contains(@text,'Street')]"));
	}
	public static WebElement Q4_Dont_Know()
	{
		return driver.findElement(By.xpath("//*[contains(@text,'Refused')]"));
	}
	public static WebElement Q5_Yes()
	{
		return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView"));
	}
	public static WebElement Q5_No()
	{
		return driver.findElement(By.xpath("(//android.widget.TextView[@text='No'])[3]"));
	}
	public static WebElement Submit_survey()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='SUBMIT']"));
	}
	public static WebElement Confirmation_ok()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
	}
	public static WebElement Confirmation_Yes()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='YES']"));
	}
	
	public static WebElement Q6_under_18()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Under 18']"));
	}
	public static WebElement Q7_Other_Gender()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Other Gender']"));
	}
	public static WebElement Q8_Black()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Black']"));
	}
	public static WebElement Decoy_validation_message()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='- Decoy Code']"));
	}
	public static WebElement Q1_validation_message()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='- 1. Is this person:']"));
	}
	public static WebElement Q2_validation_message()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='- 2. May I ask you just a few questions?']"));
	}
	public static WebElement Q3_validation_message()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='- 3. Did someone else ask you about your housing situation tonight?']"));
	}
	public static WebElement Q4_validation_message()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='- 4. Where are you sleeping tonight?']"));
	}
	public static WebElement Q5_validation_message()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='- 5. Is this person street homeless?']"));
	}
	public static WebElement Q6_validation_message()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='- 6. What is your age?']"));
	}
	public static WebElement Q7_validation_message()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='- 7. OBSERVE - DO NOT READ OUT LOUD']"));
	}
	public static WebElement Q8_validation_message()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='- 8. OBSERVE - DO NOT READ OUT LOUD (FILL IN ALL THAT APPLY)']"));
	}
	public static WebElement Cancel_survey()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='CANCEL']"));
	}
	public static WebElement Confirmation_Cancel_survey()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='YES, CANCEL']"));
	}
	public static WebElement Confirmation_Dont_Cancel_survey()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='DON'T CANCEL']"));
	}
	public static WebElement Options()
	{
		return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView"));
	}
	public static WebElement Options_Refresh()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Refresh']"));
	}
	public static WebElement Options_Logout()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Logout']"));
	}
	public static WebElement Options_Cancel()
	{
		return driver.findElement(By.xpath("//android.widget.TextView[@text='Cancel']"));
	}
	  public static WebElement scroll() { 
	  return  driver.findElement(MobileBy.AndroidUIAutomator(
	  "new UiScrollable(new UiSelector().resourceId(\"android:id/content\")).getChildByText("
	  + "new UiSelector().className(\"android.widget.TextView\"), \"Under 18\")"));
	 
	  
	  } public static WebElement scroll2() { 
		  return driver.findElement(MobileBy.AndroidUIAutomator(
	  "new UiScrollable(new UiSelector().resourceId(\"gov.nyc.dhs.oit.mobile.Hope2020.development:id/action_bar_root\")).getChildByText("
	  + "new UiSelector().className(\"android.widget.TextView\"), \"Under 18\")"));
	  
	  
	  }
	 
	  public static void scroll3()
	  {
		  try {
			    String scrollableList="android:id/content";
			    String elementClassName="android.widget.TextView";
			    String anyText="Under 18";

			    driver.findElement(MobileBy.AndroidUIAutomator(
			                    "new UiScrollable(new UiSelector().resourceId(\"" + scrollableList + "\")).getChildByText("
			                            + "new UiSelector().className(\"" + elementClassName + "\"), \"" + anyText + "\")"));
			 }
			catch (Exception e)
			{
			            System.out.println("Cannot scroll further");
			}
	  }
	  public static void scroll4()
	  {
		  try {
			    String scrollableList="gov.nyc.dhs.oit.mobile.Hope2020.development:id/action_bar_root";
			    String elementClassName="android.widget.TextView";
			    String anyText="Female";

			    driver.findElement(MobileBy.AndroidUIAutomator(
			                    "new UiScrollable(new UiSelector().resourceId(\"" + scrollableList + "\")).getChildByText("
			                            + "new UiSelector().className(\"" + elementClassName + "\"), \"" + anyText + "\")"));
			 }
			catch (Exception e)
			{
			            System.out.println("Cannot scroll further");
			}
	  }
	
}
